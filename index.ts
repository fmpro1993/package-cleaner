import axios from 'axios';
import * as fs from 'fs';
import * as dotenv from 'dotenv';
import { Command, Option } from 'commander';

const program = new Command();
dotenv.config();

const projectId = process.env.PROJECT_ID
const registryUrl = `https://gitlab.com/api/v4/projects/${projectId}/packages?per_page=100`;
const deleteUrl   = `https://gitlab.com/api/v4/projects/${projectId}/packages/`;

interface Config {
  regexPattern: string;
  deleteMatching: boolean;
}

async function getPackagesFromGitLabRegistry(): Promise<{ name: string; version: string; id: string }[]> {
  try {
    const response = await axios.get(registryUrl);
    return response.data.map((pkg: any) => ({ name: pkg.name, version: pkg.version, id: pkg.id }));
  } catch (error: any) {
    console.error('Error fetching packages:', error.message);
    throw error;
  }
}

function filterPackages(packages: { name: string; version: string; id: string }[], regex: RegExp, deleteMatching: boolean): { name: string; version: string; id: string }[] {
  if (deleteMatching) {
    return packages.filter(pkg => regex.test(pkg.name) || regex.test(pkg.version));
  } else {
    return packages.filter(pkg => !regex.test(pkg.name) && !regex.test(pkg.version));
  }
}

async function deletePackages(packagesToDelete: { name: string; version: string; id: string }[]): Promise<void> {
  try {
    const headers = {
      'PRIVATE-TOKEN': process.env.GITLAB_PRIVATE_TOKEN,
    }
    for (const pkg of packagesToDelete) {
      // await axios.delete(`${deleteUrl}${pkg.id}`, { headers: headers });
      console.log(`Deleting package: id ${pkg.id} ${pkg.name}@${pkg.version}`);
    }
  }catch (error: any) {
    console.error('Error deleting packages:', error.message);
    throw error;
  }
}

function getConfig(): Config {
  program
    .name('package-cleaner-cli')
    .description('CLI to delete packages matching regex.')
    .version('0.1.0');

  program
    .option('-r, --regex <pattern>', 'Regex pattern to match packages')
    .addOption(new Option('-c, --bool <value>', 'Delete all packages matching regex or delete all not matching regex (true or false)').choices(['true', 'false']))
    .parse(process.argv);

  const options = program.opts();
  const configFile = 'config.json';

  if (options.regex && options.bool) {
    return {
      regexPattern: options.regex,
      deleteMatching: options.bool.toLowerCase() === 'true',
    } as Config;
  }

  if (fs.existsSync(configFile)) {
    const configFromFile: Partial<Config> = JSON.parse(fs.readFileSync(configFile, 'utf-8'));
    if (configFromFile.regexPattern) {
      return {
        regexPattern: configFromFile.regexPattern,
        deleteMatching: options.bool.toLowerCase() === 'true',
      } as Config;
    }
  }

  if (process.env.REGEX_PATTERN) {
    return {
      regexPattern: process.env.REGEX_PATTERN,
      deleteMatching: options.bool.toLowerCase() === 'true',
    } as Config;
  }

  console.error('Error: regexPattern is not provided.');
  process.exit(1);
}

async function main() {
  try {
    const config: Config = getConfig();
    console.log(config)
    const packages = await getPackagesFromGitLabRegistry();
    const regex = new RegExp(config.regexPattern);
    const packagesToDelete = filterPackages(packages, regex, config.deleteMatching);
    await deletePackages(packagesToDelete);
  } catch (error: any) { 
    console.error('Script execution failed:', error.message);
    process.exit(1);
  }
}

main();